const createServer = require("./create_server.js");
const fs = require('fs');

const get = (request, response) => {
  // ...📝

  response.send(
    "200"
    // ...,
    // ...
  );
};

const post = (request, response) => {
  response.send(
    "200"
    {"Content-Type": "text/html"}
    JSON.stringify({
      message "No se que hacer"
    })
  );
};

const requestListener = (request, response) => {
  switch (request.method) {
    case "GET": {
      return getPuntuacion(request, response);
    }
    case "POST": {
      return setPuntuacion(request, response);
    }
    default: {
      return response.send(
        404,
        { "Content-Type": "text/plain" },
        "The server only supports HTTP methods GET and POST"
      );
    }
  }
};

const server = createServer((request, response) => {
  try {
    return requestListener(request, response);
  } catch (error) {
    console.error(error);
    response.send(500, { "Content-Type": "text/plain" }, "Uncaught error");
  }
});

server.listen(8080);

const getPuntuacion = (request, response) => {
  fs.readFile('./puntuacion.json', function (err,file) {
   if (err) {
      response.send(
        404,
        {"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"},
        JSON.stringify({ marcador : null })
      );
   }
  
    response.send(
      "200",
      {"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"}, 
      file
    );
  });
};

const setPuntuacion = (request, response) => {
  fs.readFile('./puntuacion.json', function read(err, file) {
    if (err){
      response.send(
        404,
        {"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"}
      );
   }

    const marcador = JSON.parse(file);
    const rojo = JSON.parse(request.body).rojo;
    const amarillo = JSON.parse(request.body).amarillo;

    marcador.push({ "jugadorRojo": rojo, "jugadorAmarillo": amarillo });

    fs.writeFile("scoreboard.json", JSON.stringify(scores, null, 2), function (err) {
      if (err) {
        console.log(err);
      }
    });

    response.send(
      "200",
      {"Content-Type": "application/json", "Access-Control-Allow-Origin": "*"},
      JSON.stringify(marcador)
    );
  })
};
server.listen(8080);

