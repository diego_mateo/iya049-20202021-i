const serURL = "./create_server.js";
let tabImg = { imagenURL: "imagenes/juego_de_la_oca_tablero.png" };//URL DEL TABLERO
let jugadores = [0,1];//Número de jugadores
let numTurnos = jugadores.length;
let tablero;
var contexto;//Almazena el contexto del canvas
let r,a;//Almacena la casilla actual de las fichas
let rojaX, rojaY, amarillaX, amarillaY;//Coordenadas de las fichas.
let dado;
let turno;//Quien tira el dado.
let roja = false;
let amarilla = false;
let avance = 0;//Indica el avance o retroceso de la ficha que tiene el turno.
//Variables para el marcador
let victoriasRojo = 0;
let victoriasAmarillo = 0;
let marcadorRojo, marcadorAmarillo;
//Funcion para dibujar el tablero
function iniciarTablero(){
	tablero = document.getElementById('tablero');
	tablero.width = 660;
	tablero.height = 660;
	contexto = tablero.getContext("2d");
	tabImg.imagen = new Image();
	tabImg.imagen.src = tabImg.imagenURL;
	tabImg.imagen.onload = confirmarFondo;
}

function confirmarFondo(){
	tabImg.imagenOK = true;
	dibujarFondo();
	dibujarFichaRoja();
	dibujarFichaAmarilla();
}
function dibujarFondo(){
	var contexto = this.contexto;
	contexto.drawImage(tabImg.imagen, 0,0);
}
function dibujarFichaRoja(){
	contexto.beginPath();
	contexto.arc(rojaX, rojaY, 15, 0, Math.PI *2, true);
	contexto.fillStyle = "#F00";
	contexto.fill();
	contexto.lineWidth = 3;
	contexto.strokeStyle = "black";
	contexto.stroke();
	contexto.closePath();
}
function dibujarFichaAmarilla(){
	contexto.beginPath();
	contexto.arc(amarillaX,amarillaY, 15, 0, Math.PI *2, true);
	contexto.fillStyle = "#FF0";
	contexto.fill();
	contexto.lineWidth = 3;
	contexto.strokeStyle = "black";
	contexto.stroke();
	contexto.closePath();
}
//Posiciones ficha roja.
let casillasRoja = [[],[125,590],[240,590],[300,590],[350,590],[405,590],[460,590],[518,590],[565,590],[595,560],[595,510],[595,460],[595,408],[595,353],[595,300],[595,248],[595,195],[595,142],[595,95],[565,65],[515,65],[465,65],[410,65],[360,65],[306,65],[253,65],[200,65],[147,65],[101,65],[67,94],[69,147],[69,200],[69,253],[69,308],[69,365],[69,418],[69,468],[100,495],[150,495],[203,495],[255,495],[308,495],[363,495],[417,495],[467,495],[503,465],[503,410],[503,352],[503,300],[503,245],[503,190],[470,155],[417,155],[361,155],[305,155],[250,155],[197,155],[165,197],[165,252],[165,310],[165,365],[195,407],[250,407],[310,330],[],[],[],[],[]];
//Posiciones ficha amarilla
let casillasAmarilla = [[],[125,625],[240,625],[300,625],[350,625],[405,625],[460,625],[518,625],[575,625],[630,580],[630,510],[630,460],[630,408],[630,353],[630,300],[630,248],[630,195],[630,142],[630,70],[580,30],[515,30],[465,30],[410,30],[360,30],[306,30],[253,30],[200,30],[147,30],[80,30],[35,75],[35,147],[35,200],[35,253],[35,308],[35,365],[35,418],[35,488],[80,530],[150,530],[203,530],[255,530],[308,530],[363,530],[417,530],[480,530],[540,485],[540,410],[540,352],[540,300],[540,245],[540,160],[500,120],[417,120],[361,120],[305,120],[250,120],[170,120],[128,170],[128,252],[128,310],[128,385],[170,442],[250,442],[350,330],[],[],[],[],[]];
function drawFichas(){
	var posicionRoja = casillasRoja[r];
	rojaX = posicionRoja[0];
	rojaY = posicionRoja[1];
	var posicionAmarilla = casillasAmarilla[a];
	amarillaX = posicionAmarilla[0];
	amarillaY = posicionAmarilla[1];
}
function moverFichaRoja(){
	drawFichas();
	dibujarFondo();
	dibujarFichaRoja();
	dibujarFichaAmarilla();
	alerta(" ");
}
function moverFichaAmarilla(){
	drawFichas();
	dibujarFondo();
	dibujarFichaRoja();
	dibujarFichaAmarilla();
	alerta(" ");
}
function sorteoInicial(){
	turno = Math.round(Math.random()*(numTurnos-1));
	sortearTurno();
	iniciaPartida();
}
function iniciaPartida(){
	r = 1;
	a = 1;
	drawFichas();
	dibujarFondo();
	dibujarFichaRoja();
	dibujarFichaAmarilla();
	inicioJuego();
}
//turno de juego

function sortearTurno(){
	if(turno === 0){
		roja = true;
		amarilla = false;
		cambiarJugador();
	}
	if(turno == 1){
		amarilla = true;
		roja = false;
		cambiarJugador();
	}
}
function cambiarTurno(){
	if(roja === true){
		roja = false;
		amarilla = true;
		 cambiarJugador();
	}
	else if(amarilla === true){
		amarilla = false;
		roja = true;
		 cambiarJugador();
	}
}
//Funcion para lanzar el dado y avanzar
function lanzarDado(){
	avance = Math.round(Math.random()*(6-1)+1);
	cambiarDado();
	alerta("Avanza "+ (avance) + " casillas");
	//Retrasamos el movimiento para poder leer el mensaje
	window.setTimeout("mover()",1500);
}
//Funcion para cambiar el dado segun el numero
function cambiarDado(){
	dado = document.getElementById("dadoEnJuego");
	var dadoUrl = "url(imagenes/dados_"+avance+".png)";
			dado.style.backgroundImage=dadoUrl;
}
//Representar al jugador por su color
function cambiarJugador(){
	dado = document.getElementById("dadoEnJuego");
	let mensajeTurno = document.getElementById("mensajeTurno");
	let dadoUrl;
	if(roja === true){
		mensajeTurno.innerHTML = "Turno para el rojo"
		dadoUrl = "url(imagenes/dadoRojo.png)";
		dado.style.backgroundImage=dadoUrl;
	}else if(amarilla ===true){
		mensajeTurno.innerHTML = "Turno para el amarillo"
		dadoUrl = "url(imagenes/dadoAmarillo.png)";
		dado.style.backgroundImage=dadoUrl;
	}
}
//Mover ficha al tirar los dados
function mover(){
		dado = document.getElementById("dadoEnJuego");
		cambiarJugador();

	if(roja === true){
		r += avance;
		moverFichaRoja();
		comprobarCasilla(r);
		cambiarTurno();
	}
	else if(amarilla === true){
		a += avance;
		moverFichaAmarilla();
		comprobarCasilla(a);
		cambiarTurno();
	}
}
//Funcion para el movimiento especial de las casillas
function movimientoEspecial(){
	if(roja === true){
		r += avance;
		window.setTimeout("moverFichaRoja()",1500);
		cambiarTurno();
	}
	else if(amarilla === true){
		a += avance;
		window.setTimeout("moverFichaAmarilla()",1500);
		cambiarTurno();
	}
}
//Casillas en caso de movimiento especial
function comprobarCasilla(casilla){
	if(casilla == 5){
		oca_4();
	}
	else if(casilla == 9){
		oca_5();
	}
	else if(casilla == 14){
		oca_4();
	}
	else if(casilla == 18){
		oca_5();
	}
	else if(casilla == 23){
		oca_4();
	}
	else if(casilla == 27){
		oca_5();
	}
	else if(casilla == 32){
		oca_4();
	}
	else if(casilla == 36){
		oca_5();
	}
	else if(casilla == 41){
		oca_4();
	}
	else if(casilla == 45){
		oca_5();
	}
	else if(casilla == 50){
		oca_4();
	}
	else if(casilla == 54){
		oca_5();
	}
	else if(casilla == 59){
		oca_59();
	}
	//Meta
	else if(casilla == 63){
		meta();
	}
	//Puentes
	else if(casilla == 6){
		puente_6();
	}
	else if(casilla == 12){
		puente_12();
	}
	//Dados
	else if(casilla == 26){
		dado_26();
	}
	else if(casilla == 53){
		dado_53();
	}
	//Calavera
	else if(casilla == 58){
		calavera();
	}
	//Laberinto
	else if(casilla == 42){
		laberinto();
	}
	//Casillas mas alla de la meta
	else if(casilla == 64){
		retrocede_1();
	}
	else if(casilla == 65){
		retrocede_2();
	}
	else if(casilla == 66){
		retrocede_3();
	}
	else if(casilla == 67){
		retrocede_4();
	}
	else if(casilla == 68){
		retrocede_5();
	}
}
/************ Funciones para casillas con Oca *****************************************/
function alerta(mensaje){
	var alerta;
	alerta = document.getElementById("alerta");
	alerta.innerHTML = mensaje;

}
//Funcion al caer en la casilla de oca
function oca_4(){
	alerta('"De oca a oca y tiro porque me toca"');
	avance = 4;
	movimientoEspecial();
}
function oca_5(){
	alerta('"De oca a oca y tiro porque me toca"');
	movimiento = 5;
	movimientoEspecial();
}
function oca_59(){
	alerta('"Gane"');
	avance = 4;
	movimientoEspecial();
	meta();
}
function meta(){
	alerta('"Gane!!!!!!!"');
	finPartida();
	ganador();
}
//Funcion para los puentes
function puente_6(){
	alerta('"De puente a puente y tiro porque me lleva la corriente"');
	avance = 6;
	movimientoEspecial();
}
function puente_12(){
	alerta('"De puente a puente y tiro porque me lleva la corriente"');
	avance = -6;
	movimientoEspecial();
}
//Funcion para los dados
function dado_26(){
	alerta('"De dados a dados y tiro porque me ha tocado"');
	avance = 27;
	movimientoEspecial();
}
function dado_53(){
	alerta('"De dados a dados y tiro porque me ha tocado"');
	avance = -27;
	movimientoEspecial();
}
//Funcion calavera
function calavera(){
	alerta('"Vuelves a empezar"');
	avance = -57;
	movimientoEspecial();
	cambiarTurno();
}
//Funcion para el laberinto
function laberinto(){
	alerta('"Has caido en el laberinto"');
	avance = -12;
	movimientoEspecial();
	cambiarTurno();
}
//Funciones por si te pasas de largo
function retrocede_1(){
	alerta('"Retrocede una casilla"');
	avance = -2;
	movimientoEspecial();
	cambiarTurno();
}
function retrocede_2(){
	alerta('"Retrocede 2 casillas"');
	avance = -4;
	movimientoEspecial();
	cambiarTurno();
}
function retrocede_3(){
	alerta('"Retrocede 3 casillas"');
	avance = -6;
	movimientoEspecial();
	cambiarTurno();
}
function retrocede_4(){
	alerta('"Retrocede 4 casillas"');
	avance = -8;
	movimientoEspecial();
	cambiarTurno();
	oca_59();
	alerta("Ganaste!!!");
}
function retrocede_5(){
	alerta('"Retrocede 5 casillas"');
	avance = -10;
	movimientoEspecial();
	cambiarTurno();
	calavera();
}

function sobre(){
	var elementoDadoSorteo ;
	var elementoDado ;

	elementoDadoSorteo = document.getElementById("dadoSorteo");
	elementoDado = document.getElementById("dadoEnJuego");

	elementoDadoSorteo.style.cursor ="pointer";
	elementoDado.style.cursor ="pointer";
}
//Variables y funciones que controlan los paneles
let normas;
let sorteo;
let controlarJuego;
let final;
function ocultarNormas(){
	normas = document.getElementById("normas");
	normas.style.display ="none";
}
function mostrarSorteo(){
	sorteo = document.getElementById("sorteo");
	sorteo.style.display ="block";
}
function ocultarSorteo(){
	sorteo = document.getElementById("sorteo");
	sorteo.style.display ="none";
}
function mostrarJuego(){
	controlarJuego = document.getElementById("panelJuego");
	controlarJuego.style.display = "block";
}
function ocultarJuego(){
	controlarJuego = document.getElementById("panelJuego");
	controlarJuego.style.display = "none";
}
function mostrarFinal(){
	final = document.getElementById("final");
	final.style.display = "block";
}
function ocultarFinal(){
	final = document.getElementById("final");
	final.style.display = "none";
}
function partida(){
	ocultarNormas();
	ocultarFinal();
	ocultarJuego();
	mostrarSorteo();
}
function inicioJuego(){
	ocultarSorteo();
	mostrarJuego();
}
function finPartida(){
	ocultarJuego();
	mostrarFinal();
}
//Marcador
const getPuntuacion = async () => {
	const response = await fetch(serURL, { method : 'GET' });
	let puntuacion = await response.json();
	let vicRojo = puntuacion[0].jugadorRojo;
	let vicAmarillo = puntuacion[0].jugadorAmarillo;
	victoriasRojo = puntuacion[0].jugadorRojo;
	victoriasAmarillo = puntuacion[0].jugadorAmarillo;
	alert(victoriasAmarillo);
	actualizarMarcador();
}
const actualizarPuntuacion = async (victoriasRojo, victoriasAmarillo) => {
	const response = await fetch(serURL, { method : 'POST', body : JSON.stringify({rojo : victoriasRojo, amarillo : victoriasAmarillo})});

}
function ganador(){
	let imgGanador = document.getElementById("ganador");
	imgGanador.style.backgroundImage="url('imagenes/campeon.png')";
	let jugadorGanador = document.getElementById("mensajeGanador")
	getPuntuacion();
	if(r == 63){
		jugadorGanador.innerHTML = "Gana el rojo";
		victoriasRojo = victoriasRojo + 1;
	}
	if(a == 63){
		jugadorGanador.innerHTML = "Gana el amarillo";
		victoriasAmarillo = victoriasAmarillo + 1;
	}
	actualizarMarcador();
}
function actualizarMarcador(){
	marcadorAmarillo = document.getElementById("marcadorAmarillo");
	marcadorRojo = document.getElementById("marcadorRojo");
	marcadorRojo.value = victoriasRojo;
	marcadorAmarillo.value = victoriasAmarillo;
}


